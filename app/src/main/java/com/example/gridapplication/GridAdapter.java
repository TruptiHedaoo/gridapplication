package com.example.gridapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

public class GridAdapter extends BaseAdapter {
    private Context mContext;
    private JSONArray authorList;

    public GridAdapter(Context context, JSONArray authorList) {
        this.mContext = context;
        this.authorList = authorList;
    }

    @Override
    public int getCount() {
        return authorList.length();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public JSONObject getItem(int position) {
        JSONObject currentJson = new JSONObject();
        try {
            currentJson = authorList.getJSONObject( position );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentJson;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View gridView = inflater.inflate( R.layout.grid_adapter, null );
        JSONObject SingleProduct = getItem( position );

        ImageView AuthorImageView = (ImageView) gridView.findViewById( R.id.author_image );
        TextView AuthorName = (TextView) gridView.findViewById( R.id.author_name );

        try {
            AuthorName.setText( SingleProduct.getString( "author" ) );
            new GetImageFromUrl( AuthorImageView ).execute( "https://picsum.photos/300/300?image=" + SingleProduct.getString( "id" ) );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gridView;
    }
}
